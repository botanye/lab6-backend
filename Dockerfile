FROM weather_site_system:latest AS build
RUN mkdir /weather_site
WORKDIR /weather_site
RUN git clone https://gitlab.com/botanye/lab6-backend.git ./
RUN npm install express
RUN npm install dotenv
RUN npm install -g pkg
RUN pkg -t node17-alpine-x64 -o server --public server.js
CMD cp /weather_site/server /volume/server
